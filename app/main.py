from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from datetime import date

from app.models.user import User
from app.models.salary import Salary

from app.routers.auth import router as auth_router
from app.routers.salary import router as salary_router

from app.db import engine, Base, get_db, save_object
from app.utils.auth import Auth


auth = Auth()

test_data={
    "test_user": [
        User(id=1, username="test1",hash_pass=auth.encode_password(password="test1")),
        User(id=2, username="test2",hash_pass=auth.encode_password(password="test2")),
        User(id=3, username="test3",hash_pass=auth.encode_password(password="test3")),
    ],
    "test_salary": [
        Salary(id=1, amount=50000, promotion_date=date(2023, 3, 10), user_id=1),
        Salary(id=2, amount=50000, promotion_date=date(2023, 6, 15), user_id=2),
        Salary(id=3, amount=50000, promotion_date=date(2023, 9, 20), user_id=3),
    ]
}


app = FastAPI()


@app.on_event("startup")
def startup():
    Base.metadata.create_all(bind=engine)
    db=next(get_db())
    db_users = db.query(User).all()
    if len(db_users) == 0:
        [save_object(object=i, db=db) for i in test_data["test_user"]]
    db_salary = db.query(Salary).all()
    if len(db_salary) == 0:
        [save_object(object=i, db=db) for i in test_data["test_salary"]]


origins = ['*']
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

app.include_router(router=auth_router)
app.include_router(router=salary_router)