from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from app.db import Base
from app.models.salary import Salary


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    username = Column(String, nullable=False, unique=True)
    hash_pass = Column(String, nullable=False)

    salary = relationship("Salary", back_populates="user", uselist=False)