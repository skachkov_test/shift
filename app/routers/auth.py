from fastapi import APIRouter, Depends, HTTPException
from fastapi.exceptions import HTTPException

from sqlalchemy.orm import Session

from app.utils.auth import Auth
from app.schemas.auth import Token, SignIn
from app.models.user import User
from app.db import get_db


auth = Auth()


router = APIRouter(
    tags=['Authorization'],
    prefix='/auth',
)

@router.post('/signin', response_model=Token)
def login(user: SignIn, db: Session = Depends(get_db)):
    """ Логин """
    db_user = db.query(User).filter(User.username == user.username).first()
    if (db_user is None):
        raise HTTPException(status_code=401, detail='Invalid username')
    if (not auth.verify_password(user.password, db_user.hash_pass)):
        raise HTTPException(status_code=401, detail='Invalid password')
    return {
        "token": auth.encode_token(db_user.username)
    }