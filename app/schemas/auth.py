from pydantic import BaseModel


class SignIn(BaseModel):
    username: str
    password: str


class Token(BaseModel):
    token: str