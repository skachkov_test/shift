from fastapi import HTTPException, Depends
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials

from sqlalchemy.orm import Session

from app.utils.auth import Auth
from app.db import get_db
from app.utils.user import get_username


security = HTTPBearer()
auth = Auth()


def get_current_user(db: Session = Depends(get_db), credentials: HTTPAuthorizationCredentials = Depends(security)):
    username = auth.decode_token(credentials.credentials)
    user = get_username(db, username)
    if user is None:
        raise HTTPException(status_code=402, detail='Unauthorized')
    return user