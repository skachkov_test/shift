from app.models.user import User
from sqlalchemy.orm import Session
from app.utils.auth import Auth


auth = Auth()


def get_username(db: Session, username: str) -> User:
    return db.query(User).filter(User.username == username).first()