from fastapi.testclient import TestClient

from app.main import app
from app.utils.auth import Auth


auth = Auth()
client = TestClient(app)


def test_auth():
    response = client.post(
        "/auth/signin",
        json={
            "username": "test1", 
            "password": "test1"
        },
    )
    assert response.status_code == 200
    assert auth.decode_token(response.json()["token"]) == "test1"


def test_wrong_username():
    response = client.post(
        "/auth/signin",
        json={
            "username": "wrong_username", 
            "password": "password"
        },
    )
    assert response.status_code == 401
    assert response.json() == {"detail": "Invalid username"}


def test_wrong_password():
    response = client.post(
        "/auth/signin",
        json={
            "username": "test1", 
            "password": "wrong_pass"
        },
    )
    assert response.status_code == 401
    assert response.json() == {"detail": "Invalid password"}